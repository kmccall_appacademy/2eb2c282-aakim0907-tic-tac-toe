require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player

  def initialize(player_one, player_two)
    @board = Board.new
    @player1 = player_one
    @player2 = player_two
    @player1.mark = :X
    @player2.mark = :O
    @current_player = @player1
  end

  def play
    play_turn until @board.over?
    puts @board.display_board
    puts "#{@board.winner} is the winner! Congratulations!" if @board.winner
  end

  def play_turn
    @current_player.display(@board)
    move = @current_player.get_move
    @board.place_mark(move, @current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == @player1
      @current_player = @player2
    else
      @current_player = @player1
    end
  end
end


if __FILE__ == $PROGRAM_NAME
  p1 = HumanPlayer.new("Kim")
  p2 = ComputerPlayer.new("Park")
  g = Game.new(p1, p2)
  g.play
end
