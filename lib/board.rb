class Board
  attr_reader :grid

  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    winners = [row_winner, column_winner, diagonal_winner]
    winners.compact.first
  end

  def over?
    return true if winner || @grid.none? { |g| g.include?(nil) }
    false
  end

  def display_board
    @grid.map do |row|
      row.map { |ele| ele.nil? ? "-" : ele }.join(" ")
    end.join("\n")
  end

  def empty_positions
    positions = []
    (0...@grid.length).each do |row|
      (0...@grid.length).each do |column|
        positions << [row, column]
      end
    end
    positions.select { |pos| self.empty?(pos) }
  end

  def winning_move?(pos, mark)
    place_mark(pos, mark)
    win = winner == mark
    self[pos] = nil
    win
  end

  private

  def get_winner(arr)
    winner = nil
    arr.each do |a|
      winner = a[0] if a.uniq.length == 1 && a[0] != nil
    end
    winner
  end

  def row_winner
    rows = []
    (0...@grid.length).each { |row| rows << @grid[row] }
    get_winner(rows)
  end

  def column_winner
    columns = []
    (0...@grid.length).each do |col|
      column = []
      (0...@grid.length).each do |row|
        column << @grid[row][col]
      end
      columns << column
    end
    get_winner(columns)
  end

  def diagonal_winner
    diagonal = [[],[]]
    (0...@grid.length).each do |n|
      diagonal[0] << @grid[n][n]
      diagonal[1] << @grid[n][@grid.length-1-n]
    end
    get_winner(diagonal)
  end

end
