class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name = "Frankie")
    @name = name
  end

  def mark=(mark)
    @mark = mark
  end

  def display(board)
    puts board.display_board
  end

  def get_move
    puts "Where would you like to place your mark? (ex. x_position, y_position)"
    move = gets.chomp
    until valid_move?(move)
      puts "Please enter a valid move (ex. x_position, y_position)"
      move = gets.chomp
    end
    get_pos(move)
  end

  private

  def valid_move?(move)
    move = get_pos(move)
    return false unless move.length == 2
    move.all? { |ele| (0..2).to_a.include?(ele)}
  end

  def get_pos(move)
    move.split(",").map(&:to_i)
  end
end
