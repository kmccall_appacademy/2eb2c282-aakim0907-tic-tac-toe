class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name = "Bonzo")
    @name = name
  end

  def mark=(mark)
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    @board.empty_positions.each do |pos|
      return pos if @board.winning_move?(pos, @mark)
    end
    @board.empty_positions.sample
  end
end
